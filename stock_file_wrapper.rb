class StockFileWrapper
  def initialize(stock_file_path)
    '''Constructor for the file'''
    @stock_file_path = stock_file_path
  end

  def get_file_header
    '''Returns only the file header'''
    File.readlines(@stock_file_path).first.strip
  end

  def get_file_lines_as_hash
    '''Returns each line in the file as a hash table'''
    Enumerator.new do |enum|
      open(@stock_file_path) do |sfp|
        header_items = sfp.gets.split(",")

        while true
          line = sfp.gets

          break unless line

          line_items = line.split(",")
          line_hash = Hash.new

          (0...header_items.length).each { |i|
            line_hash[header_items[i]] = line_items[i]
          }

          enum.yield line_hash
        end
      end
    end
  end

  def get_cols(key)
  	'''Gets the column data with the specified key'''
    Enumerator.new do |enum|
      self.get_file_lines_as_hash.each { |item| enum.yield item[key].to_f }
    end
  end

	def add_col(header, values, offset = 0, file)
		'''Adds a column of data to a csv file'''
    file_ar = File.readlines(file).to_a
    file_ar.first[0..-2] += ", #{header}"
    count = 0
    open(file, 'w+') { |sf| 
    	file_ar.each { |item| 
    		item.slice! "\n"
    		count += 1
    		if count >= offset 
    			sf << "#{item.chop}, #{values.fetch(count - offset - 1)}\n"
    		elsif count > 1
    			sf << "#{item},\n"
    		else
    			sf << item += "\n"
    		end
    	}
    }
  end

end
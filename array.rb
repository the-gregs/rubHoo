class Array
  # Calculates the moving average
  # given a Integer as a increment period
  def moving_average(increment = 1)
    moving_mean_ar = []
    (0..self.size - increment).each { |i| moving_mean_ar << to_ary[i, increment].average.round(3) }
    moving_mean_ar
  end

  def sum
    to_ary.inject { | sum,x | sum + x }
  end

  # Calculates the average
  def average
    (self.sum/self.size)
  end
end
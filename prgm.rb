gem 'statsample'
require_relative 'yahoo_stock_csv_fetcher'
require_relative 'stock_file_wrapper'
require_relative 'array'
require_relative 'linear_regression.rb'

def main()
  yff = YahooStockCsvFetcher.new(1, 7, 2015, 2000, 'GOOGL')
  print yff.url

  sfw = StockFileWrapper.new(yff.stock_file)
  yff.download_file

  # print "#{sfw.get_cols('Close').to_a.moving_average 20}\n"

  mv_avg_50  = sfw.get_cols('Close').to_a.moving_average  50
  mv_avg_100 = sfw.get_cols('Close').to_a.moving_average 100
  mv_avg_200 = sfw.get_cols('Close').to_a.moving_average 200

  sfw.add_col('rolling_50', mv_avg_50, 50, yff.stock_file)
  sfw.add_col('rolling_100',mv_avg_100, 100, yff.stock_file)
  sfw.add_col('rolling_200',mv_avg_200, 200, yff.stock_file)
end

if __FILE__ == $0
  main()
end
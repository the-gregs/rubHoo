require 'open-uri'

class YahooStockCsvFetcher
  attr_reader :day, :month, :url, :start_year, :end_year, :stock, :stock_file

  def initialize(day, month, to_year, from_year, stock)
    '''Constructor that requires all the information to build an appropriate url to call yahoo'''
    @day = day
    @month = month - 1
    @start_year = to_year
    @end_year = from_year
    @stock = stock

    @url = "http://ichart.finance.yahoo.com/table.csv?s=#{@stock}"
    @url << "&d=#{@month}&e=#{@day}&f=#{@start_year}&a=#{@month}&b=#{@day}&c=#{@end_year}&g=d"
    @url << '&ignore=.csv'

    @stock_file = "#{@stock}-full.csv"
  end

# region Methods

  def download_file
    '''
    Downloads the yahoo stock csv file in revered order. OrderBy => descending date
    '''
    open(@stock_file, 'w') { |sf| sf << open(@url).read }
    temp_file_array = File.readlines(@stock_file).reverse.to_a
    header = temp_file_array.last
    (temp_file_array -= [header]).unshift(header)
    open(@stock_file, 'w+') { |sf| temp_file_array.each { |element| sf << "#{element}" } }
  end 
  
# endregion

end